<?php

// run includes
include('ws/divi/functions.php');

/**
 * Set styles and javascript
 */
function ws_enqueue_css_js() {
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('theme-style', get_stylesheet_directory_uri() . '/assets/css/styles.css', array(), ws('version'));

    wp_enqueue_script('theme-js', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array('jquery'), ws('version'), true);
}
add_action('wp_enqueue_scripts', 'ws_enqueue_css_js', 99);

/**
 * Register Custom menus for site
 * @return void
 */
function ws_menus_register() {
    register_nav_menus([
        'quick-links' => __( 'Quick Links' ),
    ]);
}
add_action( 'init', 'ws_menus_register' );

/**
 * Create date shortcode for site.
 * @param array $atts
 * @return string date
 */
function ws_shortcode_date($atts) {
    $atts = shortcode_atts([
        'format'   => 'Y'
    ], $atts);

    return date($atts['format']);
}
add_shortcode('date', 'ws_shortcode_date');

function ws_display_menu_shortcode($atts) {

    $atts = shortcode_atts([
        'name'  => 'primary-menu',
    ], $atts);

    $menu_args = [
        'theme_location'    =>  $atts['name'],
        'echo'              =>  false
    ];

    return wp_nav_menu($menu_args);
}
add_shortcode('menu', 'ws_display_menu_shortcode');

/**
 * Add custom tabs Filters ePanel's localized tab names.
 *
 * @param string[] $tabs {
 *
 *     @type string $tab_slug Localized tab name.
 *     ...
 * }
 */
function ws_et_epanel_tab_names($tabs) {

    // add ws tab
    $tabs['ws'] = __('WS', 'ws');

    return $tabs;
}
add_filter('et_epanel_tab_names', 'ws_et_epanel_tab_names');

function ws_et_epanel_layout_data($options) {
    global $shortname;

    // add custom options
    $ws_settings_options = [

        // new tab
        [
            'name' => 'wrap-ws',
            'type' => 'contenttab-wrapstart',
            'desc' => esc_html__('WS', 'ws'),
        ],

        ['type' => 'subnavtab-start'],
        [
            'name' => 'ws-1',
            'type' => 'subnav-tab',
            'desc' => esc_html__('Layouts', 'ws'),
        ],
        ['type' => 'subnavtab-end'],
        [
            'name' => 'ws-1',
            'type' => 'subcontent-start',
        ],
        // options
        [
            "name" => esc_html__("404 Page layout", 'ws'),
            "id" => $shortname . "_404_layout_divi_id",
            "type" => "text",
            "desc" => esc_html__("Input the divi layout id that will output on the 404 page of the site.", 'ws'),
            "validation_type" => "number"
        ],
        // footer
        [
            'name' => 'ws-1',
            'type' => 'subcontent-end',
        ],
        [
            'name' => 'wrap-ws',
            'type' => 'contenttab-wrapend',
        ],
    ];

    return array_merge($options, $ws_settings_options);
}
add_filter('et_epanel_layout_data', 'ws_et_epanel_layout_data');

/**
 * Get Divi epanel option
 * @global type $shortname
 * @param type $option the option id
 * @return type
 */
function ws_get_epanel_option($option) {
    global $shortname;
    return et_get_option($shortname . $option);
}

/**
 * Return shortcode based on divi module epanel option
 * @param type $option
 * @return type string shortcode
 */
function ws_epanel_divi_layout($option) {
    return do_shortcode('[divimodule id="' . ws_get_epanel_option($option) . '"]');
}

/**
 * Return shortcode based on divi module epanel option
 * @param type $option
 * @return type string shortcode
 */
function ws_epanel_shortcode($option) {
    return do_shortcode(ws_get_epanel_option('_' . $option));
}
